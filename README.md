# st - simple terminal
[st](http://st.suckless.org/) is a simple terminal emulator for X.
All applied patches can be found in the `patches` directory.

## Requirements

In order to build st you need the Xlib header files.

## Installation

Edit config.mk to match your local setup.
By default, st is installed in `~/.local/`.

Afterwards enter the following command to build and install: `make clean install`.
